# SaltStack

Vendor: SaltStack
Homepage: https://www.vmware.com/

Product: SaltStack
Product Page: https://docs.vmware.com/en/VMware-vRealize-Automation-SaltStack-Config/8.11/install-configure-saltstack-config/GUID-DBE6D84B-0D4B-4747-8291-B0D80851CE62.html

## Introduction
We classify SaltStack into the CI/CD domain since SaltStack allows for remote execution of commands on multiple systems simultaneously, making it efficient for managing and configuring servers and other IT infrastructure. 

## Why Integrate
The Salt adapter from Itential is used to integrate the Itential Automation Platform (IAP) with SaltStack. With this adapter you have the ability to perform operations using concepts such as:

- Minions
- Hook
- Target


## Additional Product Documentation
The [API documents for SaltStack](https://salt-sproxy.readthedocs.io/en/latest/salt_api.html)