# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Salt System. The API that was used to build the adapter for Salt is usually available in the report directory of this adapter. The adapter utilizes the Salt API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Salt adapter from Itential is used to integrate the Itential Automation Platform (IAP) with SaltStack. With this adapter you have the ability to perform operations using concepts such as:

- Minions
- Hook
- Target


For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
